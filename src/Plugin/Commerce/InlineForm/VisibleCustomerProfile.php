<?php

namespace Drupal\commerce_customer_profile_visible\Plugin\Commerce\InlineForm;

use Drupal\commerce_order\Plugin\Commerce\InlineForm\CustomerProfile;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class VisibleCustomerProfile.
 *
 * This class isn't described as a CommerceInlineForm plugin. The reason is that
 * it overrides the original "customer_profile" plugin set by
 * commerce_order module.
 *
 * @see commerce_customer_profile_visible_commerce_inline_form_info_alter()
 */
class VisibleCustomerProfile extends CustomerProfile {

  /**
   * {@inheritdoc}
   */
  protected function shouldRender(array $inline_form, FormStateInterface $form_state) {
    return FALSE;
  }

}
