# Introduction
This module disables commerce's default behavior to hide the profile when in the checkout phase when filled.

# Description
When first entering an order, the profile form displays for the customers to type their information.
After typed though, they get displayed as HTML and the customers have to click an "Edit" button to display the form.
This module overrides commerce's profile inline form plugin and disables the default functionality completely.
